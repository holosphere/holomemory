# holoMemory

holoMemory is a collection of interlinked [holoAtoms][1] that constitutes the
[holoMind][2] which is the records of our global consciousness,
each atoms representing a elemental "Consciousness Unit" ([CU])

### pad :  <https://annuel2.framapad.org/p/holoMemory>


* [Protocols](protocols)


[1]: https://qwant.com/?q=holoAtoms+%26g
[2]: https://qwant.com/?q=holoMind+%26g
[CU]: https://qwant.com/?q=Consciousness+Unit+Partikis+%26g

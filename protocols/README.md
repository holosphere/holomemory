# holoMemory Protocols


every holoAtoms have an expiration date (default set to 1year)
such that the information that fall out of consciousness is delete

If the data is no longer in movement its get cleanup


The [HoloCompanion][1] (personal) and [holoBot][2] (system)

will notify about [holoAtoms][3] due to expire,
and a "ping" smart contract will extend the life of the atom
if attention is directed to it... otherwise it will fade out of the
akashic record (unpinned from IPFS and expired from the holoPad)
however holoGit object won't be purge unless explicitly removed.


There every holoPad has its expiration dates within it front-matter
(holoParameters)

exemple: [holoCryption](https://annuel2.framapad.org/p/holoCryption)

```
---
Name: holoCryption
Created: 2020/06/27
ExpirationDate: 2021/06/27
---


Section is holoCryption's holoGit
- README
- icon
- slide
- guide
- bugs / issues
- enquires and questions (optional)
- interoperability (reference to the corresponding holoBridge  (optional)

```


[1]: https://qwant.com/?q=holoCompanion+%26g
[2]: https://qwant.com/?q=holoBot+KIN+Smart+Contract+%26g
[2]: https://qwant.com/?q=holoAtoms+%26g


